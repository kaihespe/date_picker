module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      spacing: {
        9: '2.25rem',
        72: '18rem',
        80: '20rem',
        96: '24rem'
      },
      width: {
        'w-1/7': '14.2857%'
      },
    },
  },
  variants: {},
  plugins: [],
}
